#!/bin/sh

FILE=/bin/numbers-to-words

if [ -f "$FILE" ]; then
    rm -rf $FILE
fi

chmod -R 755 numbers-to-words

ln -s "$PWD/numbers-to-words/numbers-to-words" /bin/numbers-to-words