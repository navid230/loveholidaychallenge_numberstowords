
LoveHolidays Challenge
==============================================================

Overview
-------------------------
This CLI program allows you to convert Numbers to words. You simply input integer numbers and the program will convert the numbers to words.

Features
-------------------------
Convert numbers to words


Command-line Instructions
-------------------------

* **Prerequisites:**
    * Install [JDK-11](https://java.com) and [Maven 3.6.1](https://maven.apache.org/download.html).
    * check that you have the correct maven and java version and java in the terminal: `mvn -v`
    * You may need to set your `JAVA_HOME`.


* **Run:**
    * open a terminal and navigate to the loveHolidaysChallenge folder
    * replace <JDK-PATH> with your installed JDK path, in the command below.
    * replace <OS-TYPE> with your operating system reference (mac, linux or windows), in the command below.
    * execute the command below to build and deploy the program.
    * **Please Note**: you will need admin/root privileges to complete the execution of the command below.
      ```bash
      mvn clean install package -Djdk.path=<JDK-PATH> -DosType=<OS-TYPE> && sudo sh deploy.sh
      ```
    * Run the program using any of the commands shown below and replace <NUMBER-TO-CONVERT> with the number you wish to convert to words
      ```bash
      ./bin/numbers-to-words <NUMBER-TO-CONVERT>
      ```
      ```bash
      /bin/numbers-to-words <NUMBER-TO-CONVERT>
      ```
      ```bash
      numbers-to-words <NUMBER-TO-CONVERT>
      ```
