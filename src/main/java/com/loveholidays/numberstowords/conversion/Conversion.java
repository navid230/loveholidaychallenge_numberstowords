package com.loveholidays.numberstowords.conversion;

public interface Conversion {
    public String convertNumbersToWords(long number);
}
