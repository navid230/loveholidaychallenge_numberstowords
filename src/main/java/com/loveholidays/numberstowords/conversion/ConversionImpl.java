package com.loveholidays.numberstowords.conversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConversionImpl implements Conversion {

    private static final String[] UNITS_MAP = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
    private static final String[] TENS_MAP = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
    private static final String[] SPECIAL_UNITS = { "billion", "million", "thousand", "hundred"};

    public String convertNumbersToWords(long number) {
        String result = convertToWords(number);
        result = addSeparators(result);
        return mergeTens(result);
    }

    private String convertToWords(long number) {
        if (number == 0)
            return "zero";
        if (number < 0)
            return "minus " + convertToWords(Math.abs(number));
        String words = "";
        if ((number / 10000000) > 0)
        {
            words += convertToWords(number / 10000000) + " billion ";
            number %= 10000000;
        }
        if ((number / 100000) > 0)
        {
            words += convertToWords(number / 100000) + " million ";
            number %= 100000;
        }
        if ((number / 1000) > 0)
        {
            words += convertToWords(number / 1000) + " thousand ";
            number %= 1000;
        }
        if ((number / 100) > 0)
        {
            words += convertToWords(number / 100) + " hundred ";
            number %= 100;
        }
        if (number > 0) {
            if (number < 20) {
                words += UNITS_MAP[Long.valueOf(number).intValue()];
            } else {
                words += TENS_MAP[Long.valueOf(number).intValue() / 10];
                if ((number % 10) > 0) {
                    words += " " + UNITS_MAP[Long.valueOf(number).intValue() % 10];
                }
            }
        }
        return words;
    }

    private String addSeparators(String words) {
        String[] listOfWords = words.split("\\s+");

        List<Integer> indexes = new ArrayList<>();

        int counter = 0;
        for (String word : listOfWords) {
            if(Arrays.asList(SPECIAL_UNITS).contains(word)) {
                indexes.add(counter);
            }
            counter++;
        }

        counter = 0;
        if(indexes.size() > 1) {
            for (int index : indexes) {
                if (counter != indexes.size()-1) {
                    listOfWords[index] += ",";
                }else {
                    listOfWords[index] += " and";
                }
                counter++;
            }
        }

        if(indexes.size() == 1 && listOfWords.length > 2) {
            listOfWords[indexes.get(0)] += " and";
        }

        return String.join(" ",listOfWords);
    }

    private String mergeTens(String words) {
        //if result matches tens
        String matchingTens = Arrays.stream(TENS_MAP)
                .filter(words::contains)
                .findFirst()
                .orElse(null);

        if(matchingTens != null) {
            words = words.replace(matchingTens+" ", matchingTens+"-");
        }
        return words;
    }
}
