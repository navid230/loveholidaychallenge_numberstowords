package com.loveholidays.numberstowords.validation;

public interface Validate {
    Long validateInputToLong(String... input);
}
