package com.loveholidays.numberstowords.validation;

import java.util.Scanner;

public class InputValidation  implements Validate{

    // if the user inputs anything apart from integer/long, ask for correct input
    @Override
    public Long validateInputToLong(String... input) {
        try {
            if (input[0].equalsIgnoreCase("exit")) {
                System.exit(0);
            }
            long result = Long.parseLong(input[0]);

            if(result < 0 || result > 100000) throw new NumberFormatException("invalid input");

            return result;
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException nfe) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please input a number between 0 and 100000: (to exit simply input exit)");
            String nextInput = scanner.nextLine();
            return validateInputToLong(nextInput);
        }

    }
}
