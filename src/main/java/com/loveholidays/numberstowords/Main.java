package com.loveholidays.numberstowords;

import com.loveholidays.numberstowords.conversion.Conversion;
import com.loveholidays.numberstowords.conversion.ConversionImpl;
import com.loveholidays.numberstowords.validation.InputValidation;

public class Main {
    public static void main(String[] args) {
        long input = new InputValidation().validateInputToLong(args);

        Conversion numToWord = new ConversionImpl();

        // Display result
        System.out.println(numToWord.convertNumbersToWords(input));
    }
}