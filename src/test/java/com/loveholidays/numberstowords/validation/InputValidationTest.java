package com.loveholidays.numberstowords.validation;

import com.github.stefanbirkner.systemlambda.SystemLambda;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

public class InputValidationTest {
    @Rule
    public final TextFromStandardInputStream systemInMock
            = TextFromStandardInputStream.emptyStandardInputStream();

    @Test
    public void testShouldInputAValidValueIfSpecialCharactersHaveBeenPassed() {
        Validate validate = new InputValidation();

        systemInMock.provideLines("1");
        long result = validate.validateInputToLong("£");

        Assert.assertEquals(1L, result);
    }

    @Test
    public void testShouldInputAValidStringValueAndReceiveTheCorrectResponse() {

        Validate validate = new InputValidation();

        long result = validate.validateInputToLong("124");

        Assert.assertEquals(124L, result);
    }

    @Test
    public void shouldRequestForAValidInputIfValueIsAbove100000() {

        Validate validate = new InputValidation();

        systemInMock.provideLines("1");

        long result = validate.validateInputToLong("100001");

        Assert.assertEquals(1L, result);
    }

    @Test
    public void shouldRequestForAValidInputIfValueIsBelowZero() {

        Validate validate = new InputValidation();

        systemInMock.provideLines("1");
        long result = validate.validateInputToLong("-1");

        Assert.assertEquals(1L, result);
    }

    @Test
    public void shouldExitIfInputContainsExitReference() throws Exception {

        Validate validate = new InputValidation();

        int status = SystemLambda.catchSystemExit(() -> {
            validate.validateInputToLong("exit");
        });

        Assert.assertEquals(0, status);
    }
}