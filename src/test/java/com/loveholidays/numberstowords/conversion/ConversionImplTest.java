package com.loveholidays.numberstowords.conversion;

import junit.framework.TestCase;
import org.junit.Test;

public class ConversionImplTest extends TestCase {
    @Test
    public void testConvert0NumberToZero() {
        String expect = "zero";

        Conversion conversion = new ConversionImpl();

        assertEquals(expect, conversion.convertNumbersToWords(0));
    }

    @Test
    public void testConvert1000NumberToCorrectWords() {
        String expect = "one thousand";

        Conversion conversion = new ConversionImpl();

        assertEquals(expect, conversion.convertNumbersToWords(1000));
    }

    @Test
    public void testConvert101NumberToCorrectWords() {
        String expect = "one hundred and one";

        Conversion conversion = new ConversionImpl();

        assertEquals(expect, conversion.convertNumbersToWords(101));
    }

    @Test
    public void testConvert352NumberToCorrectWords() {
        String expect = "three hundred and fifty-two";

        Conversion conversion = new ConversionImpl();

        assertEquals(expect, conversion.convertNumbersToWords(352));
    }

    @Test
    public void testConvert12345NumbersToCorrectWords() {
        String expect = "twelve thousand, three hundred and forty-five";

        Conversion conversion = new ConversionImpl();

        assertEquals(expect, conversion.convertNumbersToWords(12345));
    }

    @Test
    public void testConverts120NumberToWords() {
        String expect = "one hundred and twenty";

        Conversion conversion = new ConversionImpl();

        assertEquals(expect, conversion.convertNumbersToWords(120));
    }

    @Test
    public void testConvertsNegativeNumbers() {
        String expect = "minus one hundred and twenty";

        Conversion conversion = new ConversionImpl();

        assertEquals(expect, conversion.convertNumbersToWords(-120));
    }
}